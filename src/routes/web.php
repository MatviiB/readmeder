<?php

Route::get('readmeder/search', function () {
    $api = 'https://packagist.org/search.json?';

    $results = file_get_contents($api . http_build_query(array_filter($_GET)));

    return response($results);
})->name('readmeder/search');

Route::get('readmeder/{author?}/{package?}/{file?}', function ($author = false, $package = false, $file = false) {

    if ($author && $package && $file) {
        if (file_exists(base_path("vendor/$author/$package/$file"))) {
            $html = (new \Parsedown())
                ->text(
                    file_get_contents(base_path("vendor/$author/$package/$file"))
                );

            return view('readmeder::package', compact('html'));
        }
    }

    $composer = json_decode(file_get_contents(base_path('composer.json')), true);

    foreach ($composer['require'] as $package => $version) {
        if (file_exists(base_path("vendor/$package/README.md"))) {
            $exploded = explode('/', $package);

            $require[$package] = [
                'author' => $exploded[0],
                'package' => $exploded[1],
                'version' => $version,
                'file' => 'README.md',
            ];

            continue;
        }
        if (file_exists(base_path("vendor/$package/readme.md"))) {
            $exploded = explode('/', $package);

            $require[$package] = [
                'author' => $exploded[0],
                'package' => $exploded[1],
                'version' => $version,
                'file' => 'readme.md',
            ];

            continue;
        }
    }

    foreach ($composer['require-dev'] as $package => $version) {
        if (file_exists(base_path("vendor/$package/README.md"))) {
            $exploded = explode('/', $package);

            $require_dev[$package] = [
                'author' => $exploded[0],
                'package' => $exploded[1],
                'version' => $version,
                'file' => 'README.md',
            ];

            continue;
        }
        if (file_exists(base_path("vendor/$package/readme.md"))) {
            $exploded = explode('/', $package);

            $require_dev[$package] = [
                'author' => $exploded[0],
                'package' => $exploded[1],
                'version' => $version,
                'file' => 'readme.md',
            ];

            continue;
        }
    }

    $dependencies = collect(glob(base_path('vendor/*/*/{README,readme}.md'), GLOB_BRACE));
    $vendor = collect();

    foreach ($dependencies as $key => $dependency) {
        $exploded = explode('/', $dependency);
        $author = $exploded[count($exploded) - 3];
        $package = $exploded[count($exploded) - 2];

        $item = collect();
        $item->author = $author;
        $item->package = $package;
        $item->file = $exploded[count($exploded) - 1];
        $item->letter = substr($author, 0, 1);

        $vendor->push($item);
    }

    $vendor = $vendor->sortBy('author');

    return view('readmeder::readmeder')->with([
        'require' => $require,
        'require_dev' => $require_dev,
        'vendor' => $vendor,
    ]);
});