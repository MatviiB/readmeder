<!DOCTYPE html>
<html>

@include('readmeder::head')

<body style="margin-top: 50px">

@include('readmeder::nav')

<section class="section">
    <div class="container">
        <h2>Dependencies</h2>
        <div class="columns is-variable is-6">
            <div class="column is-one-quarter is-3">
                <h4>Required</h4>
                @foreach($require as $package)
                    <p>
                        <a class="is-laravel" href="/{{ config('readmeder.url') }}/{{ $package['author'] }}/{{ $package['package'] }}/{{ $package['file'] }}">
                            {{ $package['author'] }}/{{ $package['package'] }}
                            <span style="float: right;">{{ $package['version'] }}</span>
                        </a>
                    </p>
                @endforeach
                <h4>Required-dev packages</h4>
                @foreach($require_dev as $package)
                    <p>
                        <a class="is-laravel" href="/{{ config('readmeder.url') }}/{{ $package['author'] }}/{{ $package['package'] }}/{{ $package['file'] }}">
                            {{ $package['author'] }}/{{ $package['package'] }}
                            <span style="float: right;">{{ $package['version'] }}</span>
                        </a>
                    </p>
                @endforeach
            </div>

            @php($let = $vendor->first()->letter)

            @foreach($vendor->split(3) as $key => $chunk)
                <div class="column is-one-quarter is-3">
                    @if($key === 0)
                        <h4>Vendor</h4>
                    @else
                        <h4>&nbsp;</h4>
                    @endif
                    @foreach($chunk as $key_d => $dependency)
                        <p @if($let !== $dependency->letter) style="margin-top: 10px;" @php($let = $dependency->letter) @endif>
                            <a class="is-laravel"
                               href="/{{ config('readmeder.url') }}/{{ $dependency->author }}/{{ $dependency->package }}/{{ $dependency->file }}">
                                {{ $dependency->author . '/' . $dependency->package }}
                            </a>
                        </p>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
</section>
</body>
</html>
