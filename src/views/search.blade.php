<h2>Search</h2>
<div class="columns is-variable is-6">
    <div class="column is-4">
        <h4>Search packages by name</h4>
        <div class="field">
            <div class="control">
                <input class="input is-medium" type="text" id="name" placeholder="By name">
            </div>
        </div>
    </div>
    <div class="column is-4">
        <h4>Search packages by tag</h4>
        <div class="field">
            <div class="control">
                <input class="input is-medium" type="text" id="tag" placeholder="By tag">
            </div>
        </div>
    </div>
    <div class="column is-4">
        <h4>Search packages by type</h4>
        <div class="field">
            <div class="control">
                <input class="input is-medium" type="text" id="type" placeholder="By type">
            </div>
        </div>
    </div>
</div>



<script>
    var inputName = document.getElementById('name');
    var inputTag = document.getElementById('tag');
    var inputType = document.getElementById('type');

    var timeout = null;
    var xhr = new XMLHttpRequest();

    inputName.onkeyup = function (e) {
        get();
    };

    inputTag.onkeyup = function (e) {
        get();
    };

    inputType.onkeyup = function (e) {
        get();
    };

    function get() {
        if (inputName.value.length === 0 && inputTag.value.length === 0 && inputType.value.length === 0) {
            return false;
        }
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            xhr.open( "GET", '{{ config('readmeder.url') }}/search?q=' + inputName.value + '&tags=' + inputTag.value + '&type=' + inputType.value, false);
            xhr.send();
            console.log(xhr.responseText);
        }, 2000);
    }
</script>