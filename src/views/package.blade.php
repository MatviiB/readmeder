<!DOCTYPE html>
<html>

@include('readmeder::head')

<body style="margin-top: 50px">

@include('readmeder::nav')

<section class="section">
    <div class="container">
        <div class="columns id-desktop">
            <div class="column is-two-third-desktop">
                {!! $html !!}
            </div>
        </div>
    </div>
</section>
</body>
</html>
