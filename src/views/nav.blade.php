<nav class="navbar is-light is-fixed-top" role="navigation" aria-label="main navigation">
    <div class="navbar-menu">
        <div class="navbar-start">
            <div class="navbar-item">
                <a class="button" href="{{ config('readmeder.nav-button.href') }}">
                    {{ config('readmeder.nav-button.text') }}
                </a>
            </div>
        </div>
    </div>
</nav>