<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Readmeder</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
    <style>
        html, body {
            color: #0d0e0e;
            font-family: 'Raleway', sans-serif;
        }
        .is-laravel {
            color: #f4645f;
        }
        h6 {
            font-size: 115%;
        }
        h5 {
            font-size: 130%;
        }
        h4 {
            font-size: 145%;
        }
        h3 {
            font-size: 160%;
        }
        h2 {
            font-size: 175%;
        }
        h1 {
            font-size: 190%;
        }
        h1, h2, h3, h4, h5, h6 {
            margin-top: 15px;
        }
    </style>
</head>